
package animal;

public class Pez extends Animal{
    
    int profMax;
    String tipo;

    public Pez(int profMax, String tipo, String especie, String alimentacion, int longevidad, int maxPeso, int maxAltura) {
        super(especie, alimentacion, longevidad, maxPeso, maxAltura);
        this.profMax = profMax;
        this.tipo = tipo;
    }

    public int obtenProfMax() {
        return profMax;
    }

    public void fijaProfMax(int profMax) {
        this.profMax = profMax;
    }

  
    
    
}
