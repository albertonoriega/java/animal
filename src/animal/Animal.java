
package animal;

public class Animal {
    String especie;
    String alimentacion;
    int longevidad;
    int maxPeso;
    int maxAltura;

    public Animal(String especie, String alimentacion, int longevidad, int maxPeso, int maxAltura) {
        this.especie = especie;
        this.alimentacion = alimentacion;
        this.longevidad = longevidad;
        this.maxPeso = maxPeso;
        this.maxAltura = maxAltura;
    }

    public Animal(String especie, String alimentacion) {
        this.especie = especie;
        this.alimentacion = alimentacion;
    }

   
    public void fijaEspyAli(String alimentacion, String especie) {
        this.alimentacion = alimentacion;
        this.especie= especie;
    }
    
    
    public void fijaLongyPeso(int longevidad, int maxPeso) {
        this.longevidad= longevidad;
        this.maxPeso= maxPeso;
    }

    public String obtenEspyAli() {
        return "La especie es: "+ this.especie+ " y la alimentación es: "+this.alimentacion;
    }
    
    
    public String obtenLongyPeso() {
        return "La longevidad es: "+ this.longevidad+ " y el peso máximo es: "+this.maxPeso;
    }
    
    public int proporcion (){
        int proporcion;
        System.out.println(this.maxAltura);
        System.out.println(this.maxPeso);
               return proporcion=  (this.maxAltura/this.maxPeso);
               
    }
    
    
    
    
    
    
}
