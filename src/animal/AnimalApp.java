
package animal;


public class AnimalApp {

    
    public static void main(String[] args) {
        
        Animal jirafa;
        Pez tiburon;
        
        jirafa= new Animal("jirafa", "hervivoro", 20, 500, 1000);
        
        jirafa.fijaEspyAli("herv", "jirafa larga");
        jirafa.fijaLongyPeso(40, 200);
        
        System.out.println(jirafa.obtenEspyAli());
        System.out.println(jirafa.obtenLongyPeso());
        
        System.out.println("La proporcion es : " + jirafa.proporcion());
        
        
        tiburon = new Pez(10, "blanco", "tiburon", "carnivoro", 10, 20, 2);
        System.out.println(tiburon.obtenProfMax());
        tiburon.fijaProfMax(5);
        System.out.println(tiburon.profMax);
            
    }
       
}
